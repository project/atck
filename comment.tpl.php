<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
<?php if ($new != ''): ?>
  <span class="new"><?php print $new; ?></span>
<?php endif; ?>
  <h3 class="title"><?php print $title; ?></h3>
<?php if ($picture) print $picture; ?>
  <span class="submitted"><?php print t('Submitted on ') . format_date($comment->timestamp, 'custom', 'F jS, Y') . t(' by '); ?> <?php print theme('username', $comment); ?></span>
  <div class="content">
    <?php print $content ?>
      <?php if ($signature): ?>
        <div class="user-signature clear-block">
          <?php print $signature ?>
        </div>
      <?php endif; ?>
  </div>
  <div class="links">
<?php print $links; ?>
  </div>
</div>