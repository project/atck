<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print atck_styles() ?>
  <!--[if IE 6]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/fix-ie-6.css";</style>
  <![endif]-->
  <!--[if IE 7]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/css/fix-ie-7.css";</style>
  <![endif]-->
  <?php print $scripts ?>
</head>

<body class="center">
<!-- This is merely a sample page.tpl.file. It may be useful as a reference for making your own page.tpl.php file with output from the grid builder -->

	<div <?php print $body_attributes; ?>>
	<!-- The next five lines are provided by the tck layout builder and will change based on the
			options you choose there -->
	<div class="fixed-xlg" id="container"><!-- fixed-lg defines overall width of the site -->

		<div class="section"><!-- 'section' class defines top row -->
			<div id="header">
				<?php if ($site_name): ?>
					<h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>
				<?php endif; ?>
				<?php if ($site_slogan): ?>
          <span id="tagline"><?php print $site_slogan; ?></span>
				<?php endif; ?>
			</div><!-- /#header -->
		
			<?php if ($primary_links): ?>
				<div id="main_menu">
				 <?php print theme('links', $primary_links); ?>
				</div> <!-- /#main_menu -->
			<?php endif; ?>
									
		<?php if ($secondary_links): ?> 
			<div id="secondary_menu"> <?php print theme('links', $secondary_links); ?></div>
			<?php endif; ?> 
		</div><!-- /section -->	
		
		<div class="section"><!-- 'section' class defines a new row -->
			<div class="classic untidy"><!-- '.classic untidy' allows columns of unequal heights -->
				<div class="layout a-b-c"><!-- 'layout' specifies how many columns the row will contain -->
					<div class="gr a"><!-- the first of three columns for this row -->
							<?php if ($search_box || $left): ?> 
							<div id="sidebar_left"> 
								<?php if ($search_box): ?> 
                  <div class="block"> <?php print $search_box; ?> </div> 
								<?php endif; ?> 
								<?php print $left; ?>
							</div><!-- /#sidebar_left --> 
							<?php endif; ?> 
					</div><!-- /gr a -->

					<div class="gr b"><!-- the second of three columns for this row -->
						  <?php if ($header || $breadcrumb): ?> 
							<div id="breadcrumb"> 
                <?php if ($breadcrumb): ?> 
                  <?php print $breadcrumb; ?> 
                <?php endif; ?> 
                <?php if ($header): ?> 
                  <?php print $header; ?> 
                <?php endif; ?> 
							</div><!-- /#breadcrumb --> 
							<?php endif; ?> 
							
							<div id="wrapper" class="clear-block"> 
								<div id="subwrapper"> 
									<div id="content"> 
										<?php if ($help): ?> 
                      <?php print $help; ?> 
										<?php endif; ?> 
										<?php if ($show_messages && $messages): ?> 
                      <?php print $messages; ?> 
										<?php endif; ?> 
										<?php if ($mission): ?> 
											<div id="mission"><?php print $mission; ?></div> 
										<?php endif; ?> 
										<?php if ($content_top):?> 
											<div id="content-top"><?php print $content_top; ?></div> 
										<?php endif; ?> 
										<?php if ($tabs): ?> 
											<div class="tabs"><?php print $tabs; ?></div> 
										<?php endif; ?> 
										<?php if ($title): ?> 
											<h1 class="title"><?php print $title; ?></h1> 
										<?php endif; ?> 
										<?php if ($content): ?> 
                      <?php print $content; ?> 
										<?php endif; ?>
									  <?php if ($content_bottom): ?>
                      <div id="content-bottom">
										<?php print $content_bottom; ?>
										</div><!--/content-bottom -->
										<?php endif; ?>
									</div><!-- /content --> 
								</div> <!-- /subwrapper -->
							</div><!-- /#wrapper -->
							
						<?php if ($footer): ?> 
              <div id="footer"> 
                <?php print $footer; ?> 
              </div><!-- /#footer -->
            <?php endif; ?> 
					</div><!-- /gr b -->

					<div class="gr c"><!-- the second of three columns for this row -->
						<?php if ($right): ?> 
							<div id="sidebar_right"> <?php print $right; ?></div><!-- /#sidebar_right --> 
						<?php endif; ?>
					</div><!-- /gr c -->
				</div><!-- /layout a-b-c --><br class="shim" />
			</div><!-- /classic untidy -->
		</div><!-- /section -->

  <?php if ($closure): ?> 
    <?php print $closure; ?> 
  <?php endif; ?> 

	</div><!-- /fixed-xlg container -->
</div><!-- style-wrappers -->
</body>
</html>