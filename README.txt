Instructions:

1. After downloading the atck project from Drupal.org you will need to access OR download the grid builder and the style.css file separately. They cannot be hosted on Drupal.org because they are not gpl licensed. (BSD and MIT)

Access the gridbuilder online: http://atck.highervisibilitywebsites.com/builder/

...or download the grid builder so you can run it locally (you'll need to set up a virtual host): http://atck.highervisibilitywebsites.com/atck-builder.zip

2. Download the style.css file, and place in your atck theme folder:
http://highervisibilitywebsites.com/downloads/style.css.zip

3. All of the files themselves are commented fairly well, please refer to them for guidance on how to implement things. Use the included sample page.tpl.php file to model your own page.tpl.php file, using the output the grid builder provided.

====

ATCK (Advance Theme Construction Kit)

atck was developed in a production environment for purposes of quickly building css and xtml valid Drupal themes from scratch without having to un-theme an existing Drupal theme to do it.

atck components:

1. A browser-based grid builder which produces starter code for a page.tpl.php file. The grid builder itself was originially built by Christos Constandinou (http://layout.constantology.com/) and it uses a modified version of Yahoo Grids which is more flexible and easier to customize. With Christos' permission (thanks, Christos!) it, and it's supporting css, has been customized so that it works for for Drupal themes and so that it is css3 valid. [access online or separate download]

2. style.css [separate download]
style.css contains only the css needed to supports what grid buider, as well as some 'resets'. The only modifications one should need to make here would be if they want to make their layout a fixed width and/or a different width (default widths are in %).

3. page-layout.css and template.php
These files are where the visual styling of the site happens. The source of these files is from a combination of code from the Hunchbaque theme and some changes/additions which I added in order to provide baseline settings I prefer and/or provide more granular settings. The beauty of these files is the simplicty of them - they include as little markup/styling as possible to avoid complexity, while at the same time putting many helpful tools/comments at your finger tips so that you can accomplish what you want.

4. page.tpl.php
A sample page.tpl.file is included as a reference for finishing your own page.tpl.php file using code output from the grid builder. Note particularly the variable names for the blocks, regions, menus, etc. (at this point the builder does not include those items)

5. fix-ie-6.css and fix-ie-7.css
These files are included for purposes of providing IE-only css to each of the respective versions. By using conditional comments like this we keep the main css files hack-free and atck css3 valid.

===

atck maintained by:
Caleb Gilbert (http://highervisibilitywebsites.com) 