<?php

// Nearly all of this is directly from the Hunchbaque theme, which in turn
// borrowed several things from Zen. Isn't open-source-evolution awesome!! :-)
// (thank you very much to the authors of both of those projects)
//////////////////////////////////////////////////////////////////////////////////////////

//  Overriding existing css is often much more of a pain than just figuring out how to
//  style something custom/new. Therefore we disable a lot of the core css so that we spend 
//  less time 'undo-ing' and more time 'doing'. The only ones left are the 
//  admin.css and system.css.

//  The format below should be pretty easy to figure out so you can even remove 
//  other module's default styles if you find that they are difficult to 
//  override. (Or for that matter, put one back in if you like the drupal 
//  stuff.) I have tried to put the most crucial styles into this theme's 
//  style.css so that you have a good starting point though. I even included 
//  the .clear-block class sinc ethat is a very handy class to have.

//  One other caveat to this approach is that if you want to use the non-drupal 
//  style method, you have to use atck_styles() in your page.tpl.php 
//  instead of the common $styles veriable. On the plus side, you can simply 
//  use the $styles variable if you don't like the approach here. Choice is good.

function atck_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

function atck_styles() {
  $css = drupal_add_css(path_to_theme() .'/css/style.css', 'theme', 'all');
  $css = drupal_add_css(path_to_theme() .'/css/page-layout.css', 'theme', 'all');
  $css = drupal_add_css();
  unset($css['all']['module']['modules/node/node.css']);
  unset($css['all']['module']['modules/system/defaults.css']);
  unset($css['all']['module']['modules/system/system.css']);
  unset($css['all']['module']['modules/system/system-menus.css']);
  unset($css['all']['module']['modules/user/user.css']);
  return drupal_get_css($css);
}

function atck_preprocess_page($vars) {
    
  // Determine if the page is the front page and apply pertinent classes 
  // if it is. Otherwise use that arg variables to construct the class and
  // id names.
  switch (TRUE) {
    case ($vars['is_front']):
      $body_id = 'id="front-page"';
      $body_class[] = 'front';
      break;

    case (!$vars['is_front']):
      $body_class[] = 'not-front';
      break;
  }
  
  switch (TRUE) {
    case (!arg(0)):
      $body_id = 'id="error-page"';
      $body_class[] = 'is-error';
      break;

    case (!$vars['is_front']):
      $path_alias = drupal_get_path_alias(arg(0).'/'.arg(1));
      $body_id = 'id="'.atck_id_safe($path_alias).'-page"';
      $path_explode = explode('/', $path_alias);
      $body_class[] = $path_explode[0].'-section';
      break;
  }
  
  // Check the logged in state, and add the appropriate class if so.
  if ($vars['logged_in']) {
    $body_class[] = 'logged-in';
  }
  
  // If we are looking at a full node view, construct a class to specify 
  // the node type.
  if (isset($vars['node'])) {
    $body_class[] = 'ntype-'.atck_id_safe($vars['node']->type);
  }
  
  // If editting a node, add a special class just for that.
  if (arg(2) == 'edit') {
    $body_class[] = 'edit';
  }
  
  // Normally Drupal can give me all the classes I need for the body, but 
  // beings I use a non-standard regions setup, we have to make our own.
  switch (TRUE) {
    case ($vars['main_supplements'] && $vars['secondary_supplements']):
      $body_class[] = 'sidebars';
      break;
    
    case ($vars['main_supplements']):
      $body_class[] = 'main-sidebar';
      break;
      
    case ($vars['secondary_supplements']):
      $body_class[] = 'secondary-sidebar';
      break;
    
    default:
      $body_class[] = 'no-sidebars';
      break;
  }
  
  // Now we take all those classes and ids that were created for the body 
  // and compile them into a single variable.
  $vars['body_attributes'] = $body_id.' class="'.implode(' ', $body_class).'"';
}

/**
  The following function compiles classes and ids for the individual nodes
  and then loaded them into a $attributes variable for the template.
  */

function atck_preprocess_node($vars) {
  $node_id = 'node-'.$vars['type'].'-'.$vars['nid'];
  $node_class[] = 'node';
  $node_class[] = 'ntype-'.$vars['type'];
  if ($vars['teaser']) {
    $node_class[] = 'teaser';
  }
  if (!$vars['status']) {
    $node_class[] = 'not-published';
  }
  if ($vars['promote']) {
    $node_class[] = 'promoted-to-front';
  }
  if ($vars['sticky']) {
    $node_class[] = 'sticky';
  }
  $node_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$node_id.'" class="'.implode(' ', $node_class).'"';
}

/**
  Comments need proper ids and classes too. This is pretty much the same 
  process as the page and nodes.
  */

function atck_preprocess_comment($vars) {
  $comment_id = 'node-'.$vars['node']->nid.'-comment-'.$vars['comment']->cid;
  $comment_class[] = 'comment';
  if ($vars['comment']->uid == $vars['node']->uid) {
    $comment_class[] = 'author';
  }
  if ($vars['comment']->status) {
    $comment_class[] = 'not-published';
  }
  if ($vars['comment']->new) {
    $comment_class[] = 'new';
  }
  $comment_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$comment_id.'" class="'.implode(' ', $comment_class).'"';  
}

/**
  Set the block classes.
  */

function atck_preprocess_block($vars) {
  $block_id = 'block-'.$vars['block']->module.'-'.$vars['block']->bid;
  $block_class[] = 'block';
  $block_class[] = 'btype-'.$vars['block']->module;
  $block_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$block_id.'" class="'.implode(' ', $block_class).'"';  
}

/**
  I had to wrap the profile with some extra containers to make it more 
  themeable out of the box, so I decided some extra class and id 
  information would also be handy.
  */

function atck_preprocess_user_profile($vars) {
  $profile_id = 'user-profile-'.$vars['account']->uid;
  $profile_class[] = 'user-profile';
  if ($vars['is_admin']) {
    $profile_class[] = 'administrator';
  }
  $profile_class[] = $vars['zebra'];

  $vars['attributes'] = 'id="'.$profile_id.'" class="'.implode(' ', $profile_class).'"';
}

function atck_preprocess_node_add_list($vars) {
  $ntype_add_id = 'content-add-listing';
  $ntype_add_class[] = 'content-add';
  $ntype_add_class[] = 'edit';
  
  $vars['attributes'] = 'id="'.$ntype_add_id.'" class="'.implode(' ', $ntype_add_class).'"';
  
  $vars['node_list'] = '<dl class="ntype-add-list">'."\n";
  foreach($vars['content'] as $item) {
    $vars['node_list'] .= '  <dt>'.l($item['link_title'], $item['link_path'], $item['options']).'</dt>'."\n";
    $vars['node_list'] .= '  <dd>'.$item['options']['attributes']['title'].'</dd>'."\n";
  }
  $vars['node_list'] .= '</dl>'."\n";
}
